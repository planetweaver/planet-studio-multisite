<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_ps_multi_test');

/** MySQL database username */
define('DB_USER', 'weaver');

/** MySQL database password */
define('DB_PASSWORD', '4466');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i%t]|<BWsu11MUC2;8 /R[79_ZVFh)U*F5hD:iv qfDl%Yf]x^wLUK5<A?hP`8Ve');
define('SECURE_AUTH_KEY',  '#]?j&A/mE^_%{^$?lY*Tl%7nf?j-* >_F#5;ZfXVfC;x!0X[<D7lw-Q&T*XE~D$u');
define('LOGGED_IN_KEY',    ')Hlzy.i8_Y&Iu@jQYNpJt$fYF1u_jd`fUzhZ{u(Ol@Vz@]7ERR:<SEjs?0K]!jOb');
define('NONCE_KEY',        'D!$9!O-E?He!6>j@#Tu55D%zZD6`HZwgxB?!jwN19,jAt,>FcSi)#TT3YpK7cV;}');
define('AUTH_SALT',        'J[8{3W2xd#,kt)Ne!62[2j~FbI&FBq}}gL =ZDr`dwb)=q31R3}6aQ+YNy@2.l-D');
define('SECURE_AUTH_SALT', '<Q1ab^_9/3o]K]k]:Vav:#|MuNsO4Do<V;UMN9FoQV8N:TCplH<-su47C6!{@E/f');
define('LOGGED_IN_SALT',   '1C]F7/6iJ[RqSp;W_-h]4d6#Z:,`55~{12;*}8@+Xe2A7@9mBlLXp?W8zv6-HugX');
define('NONCE_SALT',       'UL&WJHA7}=RtETWA{J;MQZA{D^{(4xGg`W[^*hP+&mabs@@(rmA85[U56e*.H}mT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'ps-multisitetest.sites.nerv');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
